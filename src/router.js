import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/repositories',
      name: 'repositories',
      component: () => import(/* webpackChunkName: "about" */ './views/Repos')
    },
    {
      path: '/contributing',
      name: 'contributing',
      component: () => import(/* webpackChunkName: "about" */ './views/Contributing')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "auth" */ './views/Auth')
    },
    {
      path: '/authorise',
      name: 'authorise',
      component: () => import(/* webpackChunkName: "authorise" */ './views/Authorise')
    },
    {
      path: '/authorise/callback',
      name: 'callback',
      component: () => import(/* webpackChunkName: "authorise" */ './views/Callback')
    },
    {
      path: '/setup',
      name: 'setup',
      component: () => import(/* webpackChunkName: "authorise" */ './views/Setup')
    },
    {
      path: '/add/repositories',
      name: 'add-repositories',
      component: () => import(/* webpackChunkName: "repos" */ './views/Repositories')
    },
    {
      path: '/add/repository/:name',
      name: 'add-respository',
      component: () => import(/* webpackChunkName: "repos" */ './views/Repository')
    }
  ]
})
