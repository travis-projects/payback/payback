import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authorise: {
      state: ''
    }
  },
  mutations: {
    setAuthorisationState: (state, token) => {
      state.authorise.state = token
    }
  },
  actions: {

  }
})
